Vue.component("tree-item", {
  template: "#item-template",
  props: {
    item: Object
  },
  data: function() {
    return {
      isOpen: false,
      details: false
    };
  },
  computed: {
    isParent: function() {
      return this.item.has_left || this.item.has_right;
    }
  },
  methods: {
    toggle_details: function(){
      this.details = !this.details;
    },
    toggle: function() {
      if (this.isParent) {
        this.isOpen = !this.isOpen;
      }
    }
  }
});

const app = new Vue({
  el: '#GenealogyTree',
  data: {
    root : {}
  },
  created(){
    /* The tree will be traversed and processed so it can match what the Vue component expects.
     * Nested functions where implemented only because the task at hand ensures there won't be any need
     * to traverse a second tree. In a more realistic environment, this functions should be defined in their
     * own class and imported. */ 
    function traverse(node, isRoot = false){
      const nnode = {}; 
      if(isRoot){
        nnode['L']=false;
        nnode['R']=false;
        nnode['isChild']=false;
      }else{
        nnode['isChild']=true;
        if(node.binary_placement=="Left")nnode['L']=true;
        else if(node.binary_placement=="Right")nnode['R']=true;
        else throw "Expected Left or Right as acceptable values for binary placement. Got: "+node.binary_placement;
      }

      for(const key in node){ /*We copy all of the Node's original attributes except for it's children. Those are processed separately.*/
        if(key!="children"){
          nnode[key]=node[key];
        }
      }
      if(!("pic" in nnode))nnode['pic']='assets/profile.jpg'; /* If the pic key is not found, a default profile picture is assigned. */
      if(node.children){
        for(const child of node.children){
          if(child.binary_placement == "Left"){
            nnode['has_left']=true;
            nnode['left']= traverse(child);
            nnode['left']['L_conn']=true;
          }
          else if(child.binary_placement == "Right"){
            nnode['has_right']=true;
            nnode['right']= traverse(child);
            nnode['right']['R_conn']=true;
          }
          else throw "Expected Left or Right as acceptable values for binary placement. Got: "+child.binary_placement;
        }
      }

      if(nnode['has_right'] && !nnode['has_left']) nnode['right']['R_conn'] = false; 
      if(!nnode['has_right'] && nnode['has_left']) nnode['left']['L_conn'] = false; 
      return nnode;
    }
    this.root = traverse(RawTree.data.attributes[0], true);

  }
}); 
