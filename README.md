# Genalogy Tree

A genealogy tree is a tool to visualize genetic and/or relational connections between individuals. 
This project is a graphical representation of Daxcsa company. Daxcsa is a direct sales company where independent distributors 
buy cryptocurrencies packages and earn a commission on products sales from people who they referred into the program.

## Features

### Navigation
You can navigate the tree using the +/- buttons located next to the name of an individual. If the button does not appear, it means
the individual has no children.

### Expandable details.
All individuals have important information that can be visualized by clicking the name of the individual. When you do, a table will appear
right below with all the details. Once you're done checking the details, you can click on the name again and the table will collapse.

### Scrollable View
The tree can easily grow both vertically and horizontally, if the tree grows too big to fit on your screen, don't worry. Appropiate 
scrollbars will appear so you can keep navigating.


## Dependencies
- This project was implemented relying primarily on Vue. Vue was implemented over CDN instead of the using the CLI assistant. 
- Fontawesome fa-icons 
### Why CDN Vue instead of CLI?
This project consisted of a very simple SPA. There's no session management and there's no navigation between multiple views. The focus was
to develop a tree view as natively as possible in order to keep it lightweight and minimize reliance on any third party code. Therefore,
CDN Vue fit perfectly.

## Demo
You can access a working demo by going to the following link: https://renato776.gitlab.io/genealogytree/

## Notes
- Lightweight. The only dependencies are Fontawesome and Vue. The rest was implemented by hand, therefore the final result is practical
and lightweight.
- The data was loaded from a Javascript file which was obtained by simply turning the JSON file into a JS object. This was possible only 
because of their inherent nature. If another thecnology other than JS were to be used for the implementation, proper parsing directly from
the JSON file would've been required.
- No JQuery. All was implemented with Vue.
- No Bootstrap. All was implemented using native CSS.
- The root of the tree has a non-existent parent. Since the root can have no parents, it was ommited from the tree.
- The root of the tree was found under data > attributes > [0]. It does not support multiple trees.
- Each node can optionally include a "pic" attribute which should contain a URL to an image to be used as a profile picture. If the 
attribute is ommited, a deafult picture will be used.

